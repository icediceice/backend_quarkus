package com.example.quarkus;


import org.eclipse.microprofile.rest.client.annotation.RegisterProvider;
import org.eclipse.microprofile.rest.client.ext.ResponseExceptionMapper;
import org.jboss.logging.Logger;

import jakarta.inject.Inject;
import jakarta.ws.rs.core.Response;
import jakarta.ws.rs.ext.Provider;

@Provider
public class ClientResponseExceptionMapper implements ResponseExceptionMapper<RuntimeException>  {
    @Inject
    Logger logger;
    
    @Override
    public RuntimeException toThrowable(Response response) {
        logger.error("Return Code: " + response.getStatus());
        // throw new RuntimeException("The remote service responded with HTTP "+response.getStatus());
        return null;
    }
    
}
